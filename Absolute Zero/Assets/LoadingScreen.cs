﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour
{
    float currTime;
    float startTime = 5;

    public Text countDownText;
        
    void Start()
    {
        currTime = startTime;
    }

    // Update is called once per frame
    void Update()
    {
        currTime -= 1 * Time.deltaTime;
        countDownText.text = currTime.ToString("0");
        if(currTime < 0)
        {
            SceneManager.LoadScene("MarcoSampleScene");
        }
    }
}
