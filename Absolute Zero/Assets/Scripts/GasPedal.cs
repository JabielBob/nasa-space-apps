﻿using UnityEngine;
using System.Collections;

public class GasPedal : MonoBehaviour {
	public GameObject car;
	public Rigidbody2D rigid_body;
	public int thrust;

	private Vector3 position;
	//Use this for initialization
	void Start () {
		rigid_body = car.GetComponent<Rigidbody2D>();
		position = new Vector3(2.0f, 0.0f, 0.0f);
	}
	
	void Update () {

	}

	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetMouseButtonDown(0)) {
		    Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		    Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

		    RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

			if (hit.collider != null) {
				rigid_body.AddForce(transform.right*50);
			} 
		}
	}

}
