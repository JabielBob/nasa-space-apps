﻿using UnityEngine;
using System.Collections;

public class Segregate : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D trash) {

		if (gameObject.tag == "Compostable") {
			if (trash.tag == "Compostable") {
				Destroy(trash.gameObject);
			} else {
				// Game Over Script
			}
		} else if (gameObject.tag == "Recyclable") {
			if (trash.tag == "Recyclable") {
				Destroy(trash.gameObject);
			} else {
				// Game Over Script
			}
		} else if (gameObject.tag == "Residual") {
			if (trash.tag == "Residual") {
				Destroy(trash.gameObject);
			} else {
				// Game Over Script
			}
		}
	}
}
