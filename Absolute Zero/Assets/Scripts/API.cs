﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;

public class API : MonoBehaviour
{
    public double temperature {get;private set;}

    // Start is called before the first frame update
    public void Start()
    {
        var startDateParameters = string.Empty;
        var latitudeParameters = "7.072222";
        var longitudeParameters = "125.613056"; // currently only supports 1 coordinate, user-specific geocoordinates will be implemented once game is ported to mobile
        var currentDate = DateTime.Today;
        var startDay = currentDate.DayOfYear - 30;
        if (startDay < 0)
        {
            startDay = 365 + startDay;
            var startYear = currentDate.Year - 1;
            startDateParameters = "A" + startYear + string.Format("{0:000}", startDay);
        }
        else startDateParameters = "A" + currentDate.Year + string.Format("{0:000}", startDay);
        var endDateParameters = "A" + currentDate.Year + string.Format("{0:000}", currentDate.DayOfYear);
        var APIConnector = new APIConnector(latitudeParameters, longitudeParameters, startDateParameters, endDateParameters); //TODO: 500 Internal Server Error on call 
        
        var subsetA = new Subset 
        { 
        	modis_date = "A2019265", 
        	calendar_date = "2019-09-22", 
        	band = "LST_Day_1km", 
        	tile = "h30v08", 
        	proc_date = "2019275210209", 
        	data = new List<int>() { 15453, 15532, 15579, 15491, 15550, 15590, 15443, 15501, 15612 } 
        }; //data today
        var subsetB = new Subset 
        { 
        	modis_date = "A2019273", 
        	calendar_date = "2019-09-30", 
        	band = "LST_Day_1km", 
        	tile = "h30v08", 
        	proc_date = "2019283072542", 
        	data = new List<int>() { 15361, 15453, 15301, 15344, 15157, 14952, 15279, 15032, 14952 } 
        };
        var data = new RootObject
        {
            xllcorner = "13859536.60",
            yllcorner = "784851.70",
            cellsize = 926.6254330558338,
            nrows = 3,
            ncols = 3,
            band = "LST_Day_1km",
            units = "Kelvin",
            scale = "0.02",
            latitude = 7.072222,
            longitude = 125.613056,
            header = "https://modis.ornl.gov/rst/api/v1/MOD11A2/subset?latitude=7.072222&longitude=125.613056&band=LST_Day_1km&startDate=A2019262&endDate=A2019292&kmAboveBelow=1&kmLeftRight=1",
            subset = new List<Subset>() { subsetA, subsetB }
        };
        var indexGenerator = new IndexGenerator(data);
        temperature = indexGenerator.TemperatureFunction();

    }

    // Update is called once per frame
    void Update()
    {

    }

    
    class Subset
    {
        public string modis_date { get; set; }
        public string calendar_date { get; set; }
        public string band { get; set; }
        public string tile { get; set; }
        public string proc_date { get; set; }
        public List<int> data { get; set; }
    }
   
    class RootObject
    {
        public string xllcorner { get; set; }
        public string yllcorner { get; set; }
        public double cellsize { get; set; }
        public int nrows { get; set; }
        public int ncols { get; set; }
        public string band { get; set; }
        public string units { get; set; }
        public string scale { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string header { get; set; }
        public List<Subset> subset { get; set; }
    }


    class IndexGenerator
    {
        public RootObject Data{ get; private set; }
        public IndexGenerator(RootObject data)
        {
            Data = data;
        }

        public double TemperatureFunction()
        {
            var averageList = new List<double>();
            var grandTotal = 0d;
            var dataAverage = 0d;
            foreach (var subset in Data.subset)
            {
                var total = 0d;
                var numberOfItems = subset.data.Count;
                foreach (var data in subset.data)
                {
                    total += data;
                }
                var subsetAverage = total / numberOfItems;
                averageList.Add(subsetAverage);
            }
            foreach (var number in averageList)
            {
                grandTotal += number;
            }
            dataAverage = grandTotal / averageList.Count;
            var averageTemperature = Math.Truncate((dataAverage * 0.02) - 273);
            return averageTemperature;
        }
        
    }

    class APIConnector
    {
        public string LatitudeParameters { get; private set; }
        public string LongitudeParameters { get; private set; }
        public string StartDateParameters { get; private set; }
        public string EndDateParameters { get; private set; }
        public RootObject Data { get; private set; }

        public APIConnector(string latitudeParameters,string longitudeParameters,string startDateParameters, string endDateParameters)
        {
            LatitudeParameters = latitudeParameters;
            LongitudeParameters = longitudeParameters;
            StartDateParameters = startDateParameters;
            EndDateParameters = endDateParameters;
            Main();
        }
        
        void Main()
        {
            string resource = "https://modis.ornl.gov/rst/api/v1/MOD11A2/subset?latitude="+LatitudeParameters+"&longitude="+LongitudeParameters+"&band=LST_Day_1km&startDate="+StartDateParameters+"&endDate="+EndDateParameters+"&kmAboveBelow=1&kmLeftRight=1";
            string urs = "https://urs.earthdata.nasa.gov";
            string username = "mantotheworld";
            string password = "Absolutezero273";
            try
            {
                // Ideally the cookie container will be persisted to/from file

                CookieContainer myContainer = new CookieContainer();


                // Create a credential cache for authenticating when redirected to Earthdata Login

                CredentialCache cache = new CredentialCache();
                cache.Add(new Uri(urs), "Basic", new NetworkCredential(username, password));


                // Execute the request

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource);           
                request.Method = "GET";
                request.Credentials = cache;
                request.CookieContainer = myContainer;
                request.PreAuthenticate = false;
                request.AllowAutoRedirect = true;
              
                using (var response = (HttpWebResponse)request.GetResponse())
                {   
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        var objText = reader.ReadToEnd();
                        Data = (RootObject)JsonUtility.FromJson(objText, typeof(RootObject));
                        foreach (var itemA in Data.subset)
                        {
                            foreach (var itemB in itemA.data)
                            {
                                Debug.Log(itemB);
                            }
                        }

                    }

                }
                Debug.Log("end");   
          
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    string error = new System.IO.StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                    Debug.Log(error);
                }
                
            }
            catch(Exception e)
            {
                Debug.Log("Error : " + e.Message);
            }
        }
    }



}
