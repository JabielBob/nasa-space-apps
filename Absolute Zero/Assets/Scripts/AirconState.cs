using UnityEngine;
using System.Collections;

//import System;

public class AirconState : MonoBehaviour {
	public GameObject indicator_light_on;
	public GameObject indicator_light_off;

	private int hour;

	void Start() {
		hour = System.DateTime.Now.Hour;
		if (hour<10) {
			indicator_light_off.SetActive(false);
		} else if(hour<22){
			indicator_light_on.SetActive(false);
		} else {
			indicator_light_off.SetActive(false);
		}
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetMouseButtonDown(0)) {
		    Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		    Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

		    RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
			if(hit.collider != null && hit.collider.gameObject.name == "AirconOn") {
				indicator_light_on.SetActive(true);
				indicator_light_off.SetActive(false);
				Debug.Log("WHY");
			} else if(hit.collider != null && hit.collider.gameObject.name == "AirconOff") {
				indicator_light_on.SetActive(false);
				indicator_light_off.SetActive(true);
			} 
		}
	}
}


