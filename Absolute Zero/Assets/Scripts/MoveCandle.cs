﻿using UnityEngine;
using System.Collections;

public class MoveCandle : MonoBehaviour {

	void FixedUpdate () {
		if (Input.GetMouseButtonDown(0)) {
		    Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		    Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

		    RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
			Debug.Log(hit);
			if(hit.collider != null) {
				Destroy(GameObject.Find(hit.collider.gameObject.name));	
			}
		}
	}
}
