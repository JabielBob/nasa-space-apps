﻿using UnityEngine;
using System.Collections;

public class FlipSwitch : MonoBehaviour {
	public GameObject day;
	public GameObject night;

	private bool switch_state;
	private int hour;
	// Use this for initialization
	void Start () {
		hour = System.DateTime.Now.Hour;
		if (hour<6) {
			day.SetActive(false);
			switch_state = false;
		} else if(hour<18){
			night.SetActive(false);
			switch_state = true;
		} else {
			day.SetActive(false);
			switch_state = false;
		}
	}
	
	// Update is called once per frame
	void Update () {	
	   if (Input.GetMouseButtonDown(0)) {
	   		if (switch_state == true) {
	   			day.SetActive(false);
	   			night.SetActive(true);
				switch_state = false;
	   		} else {
				day.SetActive(true);
				night.SetActive(false);
				switch_state = true;
	   		}
	   }
	}
}
