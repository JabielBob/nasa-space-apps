﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerSlider : MonoBehaviour
{
    public Slider slider;
    private float timeRemaining;
    private const float startTime = 10f;
    bool hasStarted = false;
    // Update is called once per frame

    void Update()
    {
        
        slider.value = CalculateSliderValue();
        if (!hasStarted)
        {
            timeRemaining = startTime;
            hasStarted = true;
        }
        if (timeRemaining <= 0)
        {
            timeRemaining = 0;
            Debug.Log("Time is up!");
            enabled = false;
        }
        else if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
        }
        
    }

    float CalculateSliderValue()
    {
       return   timeRemaining / startTime;
    }
}
